public class Lance {

    private Usuario usuario;
    private double valorDoLance;

    public Lance(){}

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public double getValorDoLance() {
        return valorDoLance;
    }

    public void setValorDoLance(double valorDoLance) {
        this.valorDoLance = valorDoLance;
    }
}
